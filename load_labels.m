function [labels, labels0] = load_labels(imsdir, ims_names, labdir, lab_names, rez)
N = length(lab_names);

labels = cell(N,1);
labels0 = cell(N,1);

fprintf('loading labels...\n');
for n=1:N
    
    path = [imsdir filesep ims_names(n).name];
%     path = sprintf('%s/%s', imsdir, ims_names(n).name);

    [ts, im] = pedo_extract(path);
    
    im = padarray(im, [5,5]);
    for k=1:length(ts)
        ts{k} = padarray(ts{k}, [5,5]);
    end

    
    lab = importdata([labdir lab_names(n).name]);
    
        lab = padarray(lab, [5,5]);

    lab = crop_labels_to_foot(im, lab);
    
    %%optional: calculate region 5
    lab = compute_region_5(im, lab);
    
    
    labels0{n} = max(0,lab+1);
    labels{n} = imresize(labels0{n},rez,'nearest');

end
end