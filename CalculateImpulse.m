function [ Impulse, Map ] = CalculateImpulse( TimeSeriesData )
%% CalculateImpulse
%   Given an input TSD, return the calculated total impulse as well as a
%   map describing the impulse for each pixel. Based on pedo_mask.m written
%   by Quinn Boser.

% Area in m^2
cell_area = 0.005 * 0.005 * 1000;
% Time in s
time = 10 / 1000;

[M, N] = size(TimeSeriesData{1});

Map = zeros(M, N);

% Iterate over each pixel
for i = 1 : M
    for j = 1 : N
        x = zeros(1, numel(TimeSeriesData));
        y = linspace(0, time * numel(TimeSeriesData) - time, numel(TimeSeriesData));
        
        % Collect the time series for this pixel
        for t = 1 : numel(TimeSeriesData)
            img = TimeSeriesData{t} .* cell_area;
            x(t) = img(i, j);
        end
        
        % Calculate the impulse using Simpson's Rule
        Map(i, j) = simps(y, x);
    end
end

% Calculate the total impulse.
Impulse = sum(sum(Map));

end
