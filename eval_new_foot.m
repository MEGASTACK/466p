function[x_pred, b_i, b_ij]= eval_new_foot(dir, filename)

imsdir = dir;
ims_names = [];
ims_names = [ims_names filename];

p = importdata(['trained_model' filesep 'model.mat']);

%note that these should be the same as trained model. Maybe I whould 
%load them from the report dump.
feat_params = {{'position',1},{'fourier',1},{'hog',8}};
edge_params = {{'const'},{'diffthresh'},{'pairtypes'}};
rez = 1;
rho = 0.5;
nvals = 6;

%this probably never changes
crf_type = 'linear_linear';

% load some features!
[im, tsd] = load_im(imsdir, filename);
feat = [];

[feat, im] = generate_crf_feature(feat, im, feat_params, rez);

% Next, we will make the graph structure. In this dataset, the images come 
% in slightly different sizes. Rather than making a different graph for each 
% image (which would be fine if slow) we use a "hashing" 
% strategy to make them, then copy into an array with one per image.
model_hash = repmat({[]},1000,1000);
fprintf('building models...\n')
[ly lx lz] = size(im);
if isempty(model_hash{ly,lx});
    model_hash{ly,lx} = gridmodel(ly,lx,nvals);
end
[ly lx lz] = size(im);
model = model_hash{ly,lx};

% models = make_graph_structure(im, nvals);
fprintf('computing edge features...\n')
efeats = edgeify_im(im,edge_params,model.pairs,model.pairtype);

loss_spec = 'trunc_cl_trwpll_5';

%Evaluate the new foot!
[b_i, b_ij] = eval_crf(p,feat,efeats,model,loss_spec,crf_type,rho);

[ly lx lz] = size(im);
[~, x_pred_with_padding] = max(b_i,[],1);
x_pred_with_padding = reshape(x_pred_with_padding,ly,lx);
x_pred_with_padding  = imresize(x_pred_with_padding,size(im),'nearest');
x_pred_with_padding = x_pred_with_padding - 1;
;
% remove padding added during normalization (5 on each border)
padstart = 1+5;
x_pred = x_pred_with_padding(padstart:end-5,padstart:end-5);

%miximshow(reshape(x_pred,ly-10,lx-10),nvals);
%drawnow;

end
