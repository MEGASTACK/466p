function [ output_args ] = evaluate_crf( model_p, feats_test, efeats_test, models_test )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here




fprintf('get the marginals for test images...\n');
close all
for n=1:length(feats_test)
    figure(n);
    [b_i b_ij] = eval_crf(model_p,feats_test{n},efeats_test{n},models_test{n},loss_spec,crf_type,rho);

%     [ly lx lz] = size(labels_test{n});
    [~,x_pred] = max(b_i,[],1);
    x_pred = reshape(x_pred,ly,lx);

    [ly lx lz] = size(labels0_test{n});
    x       = labels0_test{n};
    % upsample predicted images to full resolution
    x_pred  = imresize(x_pred,size(x),'nearest');
    E(n)   = sum(x_pred(x(:)>0)~=x(x(:)>0));
    T(n)   = sum(x(:)>0);

    [ly lx lz] = size(ims_test{n});
    subplot(2,3,1)
    miximshow(reshape(b_i',ly,lx,nvals),nvals);
    subplot(2,3,2)
    imshow(ims_test{n})
    subplot(2,3,3)
%     miximshow(reshape(labels_test{n},ly,lx),nvals);

%     [ly lx lz] = size(labels0_test{n});
    subplot(2,3,4)
    miximshow(reshape(x_pred,ly,lx),nvals);
    subplot(2,3,5)
    imshow(ims_test{n})
    subplot(2,3,6)
%     miximshow(reshape(labels0_test{n},ly,lx),nvals);
    drawnow
%     mkdir('results');
%     savefig(sprintf('results/classify_%d_.fig', n));
end
end

