function [ Classifier, selfAccuracy ] = GetLRClassifier( )
%% GetLRClassifier
%   Make an L/R foot classifier based on train_test_data, and get the
%   accuracy of the classifier on the training data. Not really a good
%   measure of accuracy, but still an interesting metric to know.

data = LoadPedoData('train_test_data');
labels = [linspace(1, 1, 60), linspace(2, 2, 60)];

lFeet = data(:,:,1:60);
rFeet = data(:,:,61:120);

trainingSet = cat(3, lFeet(:,:,1:50), rFeet(:,:,1:50));
testingSet  = cat(3, lFeet(:,:,1:50), rFeet(:,:,1:50));

[Classifier, hogData] = GetImageClassifier(data, labels);

correct = 0;
for i = 1 : size(hogData, 1)
    if predict(Classifier, hogData(i, :)) == labels(i)
        correct = correct + 1;
    end
end

selfAccuracy = correct / size(hogData, 1);

end

