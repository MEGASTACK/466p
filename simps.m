function I = simps( x , y )
%simps performs numerical integration on set of discrete data points using
% Simpson's Rule
% By Quinn Boser  
%   
%   Syntax: I = simps( x,y )
%   
%   Input:  x = set of evenly spaced discrete values for independent variable
%           y = set of discrete values for dependent variable
%
%   Output: I = numerical integration 

%---------------------------------------------------% 
% "Housekeeping":
%---------------------------------------------------%
if length(x)~=length(y)                 % if dimensions of x and y don't match
    disp('Error: dimension mismatch')
    ret
    urn
end

n = length(x);                          % number of data points 
if (n/2) == floor (n/2)                 % if number of of data points even
    x = [x  x(end)];                    % add dummy replicate data point to make odd
    y = [y  y(end)];                        
end

%---------------------------------------------------% 
% Numerical Integration:
%---------------------------------------------------%  
a = x(1);
b = x(end);
h = (b-a)/(n-1);                     % calc. h for equation below
I = (h/3)*(y(1)+4*sum(y(2:2:n-1))+2*sum(y(3:2:n-2))+y(n));

end

