function [im, tsd] = load_im(imsdir, filename)
    path = sprintf('%s/%s', imsdir, filename);

    [ts, im] = pedo_extract(path);
    
    %test padding
    im = padarray(im, [5,5]);
    for k=1:length(ts)
        ts{k} = padarray(ts{k}, [5,5]);
    end

    tsd = ts;
    

    im = im/255;    
end