function [ difference ] = ImpulseDifference( GroundTruth, Prediction, Impulse, Label )
%% ImpulseDifference
%   Calculates the dispairity between the predicted impulse for the region
%   with the given Label. Implulse is a matrix representing the total
%   impulse for each given pixel of GroundTruth and Prediction.
ImpTruth      = sum(sum(Impulse .* (GroundTruth == Label)));
ImpPrediction = sum(sum(Impulse .* (Prediction  == Label)));

if ImpTruth == 0
    difference = 1;
else
    difference = abs((ImpPrediction - ImpTruth) / ImpTruth);
end

end

