function [I, labels1, I_test, labels1_test] = getData()

folder_name = ['CNNresults' filesep datestr(now, 'dd-mm-yyyy_hh.MM.ss')];
mkdir(folder_name);
echo on;
diary([folder_name filesep 'log']);


imsdir = ['train_test_data' filesep];
labdir = ['labels_data' filesep];

%%
% Parameters
%
nvals  = 6;
rez    = 1; % how much to reduce resolution
rho    = 0.5; % (1 = loopy belief propagation) (.5 = tree-reweighted belief propagation)
% Next, we need to choose what features will be used. Here, we choose to use the RGB intensities, and position, jointly Fourier expanded, plus a histogram of Gaussians, computed using Piotr Dollar's toolbox.
feat_params = {{'position',1},{'fourier',1}};
% edge features
edge_params = {{'const'},{'diffthresh'},{'pairtypes'}};

foot = 'L';
samples = 'all';

% Now, we will load the data. In the backgrounds dataset, labels are stored as a text array of integers in the range 0-7, with negative values for unlabelled regions. JGMT uses 0 to represent unlabelled/hidden values, so we make this conversion when loading the data. Additionally, we reduce resolution to 20% after computing the features. This actually increases the accuracy of the final predictions, interpolated back to the original resolution.
[ims_names, lab_names] = select_data(imsdir, labdir, foot, samples);

N = length(ims_names);
[ims, feats, efeats, labels, labels0, models, tsd] = generate_crf_features(imsdir, labdir, ims_names, lab_names, feat_params, edge_params);

% Next up, we split the data into a training set (80%) and a test set (20%).
fprintf('splitting data into a training and a test set...\n')
k = 5;
[who_train, who_test] = kfold_sets(N,5,k);

I     = ims(who_train);
%feats_train   = feats(who_train);
%efeats_train  = efeats(who_train);
labels1  = labels(who_train);
%labels0_train = labels0(who_train);
%models_train  = models(who_train);

I_test     = ims(who_test);
%feats_test   = feats(who_test);
%efeats_test  = efeats(who_test);
labels1_test  = labels(who_test);
%labels0_test = labels0(who_test);
%models_test  = models(who_test);
%%

%padarray to functions. 

for i = 1:length(I);
    maxrows=60;
    maxcolumns=60;
    if size(I{i},1)<maxrows
        I{i}=padarray(I{i},[(maxrows-size(I{i},1)) 0],'post');
    end
    if size(I{i},2)<maxcolumns
        I{i}=padarray(I{i},[0 (maxcolumns-size(I{i},2))],'post');
    end
end

for i = 1:length(labels1);
    maxrows=60;
    maxcolumns=60;
    if size(labels1{i},1)<maxrows
        labels1{i}=padarray(labels1{i},[(maxrows-size(labels1{i},1)) 0],'post');
    end
    if size(labels1{i},2)<maxcolumns
        labels1{i}=padarray(labels1{i},[0 (maxcolumns-size(labels1{i},2))],'post');
    end
end

for i = 1:length(I_test);
    maxrows=60;
    maxcolumns=60;
    if size(I_test{i},1)<maxrows
        I_test{i}=padarray(I_test{i},[(maxrows-size(I_test{i},1)) 0],'post');
    end
    if size(I_test{i},2)<maxcolumns
        I_test{i}=padarray(I_test{i},[0 (maxcolumns-size(I_test{i},2))],'post');
    end
end

for i = 1:length(labels1_test);
    maxrows=60;
    maxcolumns=60;
    if size(labels1_test{i},1)<maxrows
        labels1_test{i}=padarray(labels1_test{i},[(maxrows-size(labels1_test{i},1)) 0],'post');
    end
    if size(labels1_test{i},2)<maxcolumns
        labels1_test{i}=padarray(labels1_test{i},[0 (maxcolumns-size(labels1_test{i},2))],'post');
    end
end