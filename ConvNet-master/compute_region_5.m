function [label] = compute_region_5(model, label)
%COMPUTE_REGION_5 
% set unlabeled regions that lie within the foot to class 5.
    mask = (model > 0) * 5;
    label(label == 0) = 10;
    label = min(label, mask);
    label(label == 10) = 0;
end