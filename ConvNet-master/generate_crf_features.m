function [ims, feats, efeats, labels, labels0, models, tsd] = generate_crf_features(imsdir, labdir, ims_names, lab_names, feat_params, edge_params)
% lol docstrings are for noobs

rez = 1;
nvals = 6;

N = length(ims_names);
ims    = cell(N,1);
tsd    = cell(N,1);
labels = cell(N,1);

fprintf('loading data and computing feature maps...\n');
parfor n=1:N
    % load data
    lab = importdata([labdir lab_names(n).name]);
    
%     lab = pedo_extract([labdir lab_names(n).name]);
%     im  = double(imread(([imsdir ims_names(n).name])))/255;

    path = sprintf('%s/%s', imsdir, ims_names(n).name);

    [ts, im] = pedo_extract(path);
    
    tsd{n} = ts;
    
    lab = crop_labels_to_foot(im, lab);
    
    
    %%optional: calculate region 5
    lab = compute_region_5(im, lab);
    
    im = im/255;

% %     im = double(lab)/255.;
    
    
    ims{n}  = im;
    labels0{n} = max(0,lab+1);

    % compute features
    feats{n}  = featurize_im(ims{n},feat_params);

    % reduce resolution for speed
    ims{n}    = imresize(ims{n},    rez,'bilinear');
    feats{n}  = imresize(feats{n},  rez,'bilinear');
    labels{n} = imresize(labels0{n},rez,'nearest');

    % reshape features
    [ly lx lz] = size(feats{n});
    feats{n} = reshape(feats{n},ly*lx,lz);
end
% Next, we will make the graph structure. In this dataset, the images come in slightly different sizes. Rather than making a different graph for each image (which would be fine if slow) we use a "hashing" strategy to make them, then copy into an array with one per image.
model_hash = repmat({[]},1000,1000);
fprintf('building models...\n')
for n=1:N
    [ly lx lz] = size(ims{n});
    if isempty(model_hash{ly,lx});
        model_hash{ly,lx} = gridmodel(ly,lx,nvals);
    end
end
models = cell(N,1);
for n=1:N
    [ly lx lz] = size(ims{n});
    models{n} = model_hash{ly,lx};
end
% Now, we compute edge features. (This must be done here since it uses the graph structures.) First off, we must specify what features to use. Here, we choose a constant of one, a set of thresholds on the difference of neighboring pixels, and "pairtype" features. In pairtype last ones, the number of features is doubled, with the previous features either put in the first or second half. The effect is that vertical and horizontal edges are parameterized separately.
fprintf('computing edge features...\n')
efeats = cell(N,1);
parfor n=1:N
    efeats{n} = edgeify_im(ims{n},edge_params,models{n}.pairs,models{n}.pairtype);
end
end