%main


[train_x,train_y,test_x,test_y] = getData();
funtype = 'matlab';

clear params;
params.epochs = 1;
params.alpha = 0.1;
% this is the parameter for invariant backpropagation
% keep it 0 for standard backpropagation
params.beta = 0; 
params.momentum = 0.9;
params.lossfun = 'logreg';
params.shuffle = 1;
params.seed = 0;
dropout = 0;

layers = {
  struct('type', 'i', 'mapsize', [60 60], 'outputmaps', 4)
  % remove the following layer in the Matlab version - it is not implemented there
  %struct('type', 'j', 'mapsize', [60 60], 'shift', [1 1], ...
  %       'scale', [1.40 1.40], 'angle', 0.10, 'defval', 0)  
  struct('type', 'c', 'filtersize', [4 4], 'outputmaps', 32)
  struct('type', 's', 'scale', [3 3], 'function', 'max', 'stride', [2 2])
  struct('type', 'c', 'filtersize', [5 5], 'outputmaps', 64, 'padding', [2 2])
  struct('type', 's', 'scale', [3 3], 'function', 'max', 'stride', [2 2])
  struct('type', 'f', 'length', 256, 'dropout', dropout)
  struct('type', 'f', 'length', 4, 'function', 'soft')
};

weights = single(genweights(layers, params, funtype));
EpochNum = 10;
errors = zeros(EpochNum, 1);
for i = 1 : EpochNum
  disp(['Epoch: ' num2str((i-1) * params.epochs + 1)])
  [weights, trainerr] = cnntrain(layers, weights, params, train_x, train_y, funtype);  
  disp([num2str(mean(trainerr(:, 1))) ' loss']);  
  [err, bad, pred] = cnntest(layers, weights, params, test_x, test_y, funtype);  
  disp([num2str(err*100) '% error']);  
  errors(i) = err;
  params.alpha = params.alpha * 0.95;
  params.beta = params.beta * 0.95;
end;
plot(errors);
disp('Done!');

