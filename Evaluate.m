function [ Jaccard, Rand, PXErr, ImpToe, ImpMFF, ImpLFF, ImpHeel, ImpOther ] = Evaluate( GroundTruth, Prediction, ImpulseMap, Labels )
%% Evaluate
%   Given the ground truth and predicted labelings for an image, produce a
%   bunch of different metrics that evaluate it.
%   Outputs:
%       Jaccard  - The Jaccard similarity coefficient
%       Rand     - The Rand index
%       PXErr    - The pixel-wise error (percentage of pixels that have the
%                  correct label).
%       ImpToe   - The perportional difference between the toe's impulse 
%                  calculated using the supplied ImpulseMap. See
%                  CalculateImpulse.m to get this.
%       ImpMFF   - As ImpToe, but for the medial forefoot.
%       ImpLFF   - As ImpToe, but for the lateral forefoot.
%       ImpHeel  - As ImpToe, but for the heel.
%       ImpOther - As ImpToe, but for the unaccounted impulse.

if nargin < 4
    Labels = struct(...
        'Toe',   2, ...
        'MFF',   3, ...
        'LFF',   4, ...
        'Heel',  5, ...
        'Other', 6);
end

Jaccard  =      JaccardScore( GroundTruth, Prediction );
Rand     =         RandIndex( GroundTruth, Prediction );
PXErr    =    PixelWiseError( GroundTruth, Prediction );
ImpToe   = ImpulseDifference( GroundTruth, Prediction, ImpulseMap, Labels.Toe );
ImpMFF   = ImpulseDifference( GroundTruth, Prediction, ImpulseMap, Labels.MFF );
ImpLFF   = ImpulseDifference( GroundTruth, Prediction, ImpulseMap, Labels.LFF );
ImpHeel  = ImpulseDifference( GroundTruth, Prediction, ImpulseMap, Labels.Heel );
ImpOther = ImpulseDifference( GroundTruth, Prediction, ImpulseMap, Labels.Other );

end

