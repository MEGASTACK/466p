function [ims, feats, efeats, models, tsd] = generate_crf_features(imsdir, ims_names, feat_params, edge_params, rez)
% lol docstrings are for noobs

nvals = 6;

N = length(ims_names);
ims    = cell(N,1);
tsd    = cell(N,1);
feats  = cell(N, 1);

fprintf('loading data and computing feature maps...\n');
for n=[1:N]
    % load data  
    [ims{n}, tsd{n}]  = load_im(imsdir, ims_names(n).name);
    
%     % compute features
    [feats{n}, ims{n}] = generate_crf_feature(feats{n}, ims{n}, feat_params, rez);

end
% % Next, we will make the graph structure. In this dataset, the images come in slightly different sizes. Rather than making a different graph for each image (which would be fine if slow) we use a "hashing" strategy to make them, then copy into an array with one per image.
models = make_graph_structure(ims, nvals);

% Now, we compute edge features. (This must be done here since it uses the graph structures.) First off, we must specify what features to use. Here, we choose a constant of one, a set of thresholds on the difference of neighboring pixels, and "pairtype" features. In pairtype last ones, the number of features is doubled, with the previous features either put in the first or second half. The effect is that vertical and horizontal edges are parameterized separately.
fprintf('computing edge features...\n')
efeats = cell(N,1);
parfor n=1:N
    efeats{n} = edgeify_im(ims{n},edge_params,models{n}.pairs,models{n}.pairtype);
end
end