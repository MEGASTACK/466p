function [ data ] = GetMetrics( GroundTruthPath )
%% GetMetrics

%% Apply Defaults
if nargin < 1
    GroundTruthPath = 'labels_data';
end

%% Load up the ground truths
files = dir(GroundTruthPath);
data = cell(0, 0);

for i = 1 : length(files)
    if or(strcmp(files(i).name, '.'), strcmp(files(i).name, '..'))
        continue;
    end
    
    [~, fName, ext] = fileparts([path, '/', files(i).name]);
    
    if strcmp(ext, '.mat')
        tmp = load([GroundTruthPath, '/', fName, ext]);
        data{length(data) + 1} = tmp.out_mat;
    end
end

end

