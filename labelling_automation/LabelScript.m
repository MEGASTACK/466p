dirRoot = '../FinalData/';

for i = 51 : 59
    dirName = [dirRoot, 'NP', num2str(i)];
    
    files = dir(dirName);
    
    for j = 1 : numel(files)
        [p, n, e] = fileparts([dirName, '/', files(j).name]);
        if strcmp(e, '.lst')
            label_foot(['NP', num2str(i)], n)
        end
    end
end