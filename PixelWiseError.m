function [ error ] = PixelWiseError( GroundTruth, Prediction )
%% PixelWiseError
%   Returns the pixel-wise error between the two given matricies (the
%   percentage of pixels that differ between the GroundTruth and the
%   Prediction.

diff = GroundTruth ~= Prediction;
[m, n] = size(diff);
error = sum(sum(diff)) / (m * n);

end

