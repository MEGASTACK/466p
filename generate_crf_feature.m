function[feat, im] = generate_crf_feature(feat, im, feat_params, rez)
    % compute features
    feat  = featurize_im(im,feat_params);

    % reduce resolution for speed
    im    = imresize(im,    rez,'bilinear');
    feat  = imresize(feat,  rez,'bilinear');

    % reshape features
    [ly lx lz] = size(feat);
    feat = reshape(feat,ly*lx,lz);
end