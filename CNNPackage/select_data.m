function [ images, labels ] = select_data( images_path, labels_path, foot, options)
%SELECT_DATA returns lists of data to be processed.
%   Detailed explanation goes here

all = 'all';
left = 'left';
right = 'right';
one = 'one';


all_images = dir([images_path '*.lst']);
all_labels = dir([labels_path '*.mat']);
images_names = [];
labels_names = [];
images = [];
labels = [];


if (~exist('options', 'var'))
    options = all;
end

if (~exist('foot', 'var'))
    foot = 'L';
end


%pick all feet from every subject...
if (strcmp(options, all))
    images_names = all_images;
    labels_names = all_labels;
% pick only one foot per subject
elseif (strcmp(options, one))
    
    found_im = [];
    found_lab = [];
    
    for im = all_images'
        filename = im.name;
        if(length(strfind(found_im, filename(2:5))) == 0)
            found_im = [found_im filename(2:5)];
            images_names = [images_names, im];
        end
    end
    
    for lab = all_labels'
        filename = lab.name;
        if(length(strfind(found_lab, filename(2:5))) == 0)
            found_lab = [found_lab filename(2:5)];
            labels_names = [labels_names, lab];
        end
    end
end


%Filter left/right properly
for im = images_names'
    filename = im.name;
    if(filename(1) == foot)
        images = [images, im];
    end
end

for lab = labels_names'
    filename = lab.name;
    if(filename(1) == foot)
        labels = [labels, lab];
    end
end



end

