function [ output_args ] = main( input_args )
%SOPHISTICATED_EXAMPLE Summary of this function goes here
%   Detailed explanation goes here

% set up logging and stuff
folder_name = ['results' filesep datestr(now, 'dd-mm-yyyy_hh.MM.ss')];
mkdir(folder_name);
echo on;
diary([folder_name filesep 'log']);


imsdir = ['train_test_data' filesep];
labdir = ['labels_data' filesep];

%%
% Parameters
%
nvals  = 6;
rez    = 1; % how much to reduce resolution
rho    = 0.5; % (1 = loopy belief propagation) (.5 = tree-reweighted belief propagation)
% Next, we need to choose what features will be used. Here, we choose to use the RGB intensities, and position, jointly Fourier expanded, plus a histogram of Gaussians, computed using Piotr Dollar's toolbox.
feat_params = {{'position',1},{'fourier',1},{'hog',8}};
% edge features
edge_params = {{'const'},{'diffthresh'},{'pairtypes'}};

foot = 'L';
samples = 'all';

% Now, we will load the data. In the backgrounds dataset, labels are stored as a text array of integers in the range 0-7, with negative values for unlabelled regions. JGMT uses 0 to represent unlabelled/hidden values, so we make this conversion when loading the data. Additionally, we reduce resolution to 20% after computing the features. This actually increases the accuracy of the final predictions, interpolated back to the original resolution.
[ims_names, lab_names] = select_data(imsdir, labdir, foot, samples);

N = length(ims_names);

[labels, labels0] = load_labels(imsdir, ims_names, labdir, lab_names, rez);
[ims, feats, efeats, models, tsd] = generate_crf_features(imsdir, ims_names, feat_params, edge_params, rez);


% Next up, we split the data into a training set (80%) and a test set (20%).
fprintf('splitting data into a training and a test set...\n')
k = 1;
[who_train, who_test] = kfold_sets(N,5,k);

ims_train     = ims(who_train);
feats_train   = feats(who_train);
efeats_train  = efeats(who_train);
labels_train  = labels(who_train);
labels0_train = labels0(who_train);
models_train  = models(who_train);

tsd_train = tsd(who_train);

ims_test     = ims(who_test);
feats_test   = feats(who_test);
efeats_test  = efeats(who_test);
labels_test  = labels(who_test);
labels0_test = labels0(who_test);
models_test  = models(who_test);

tsd_test = tsd(who_test);

% Again we make a visualization function. This takes a cell array of predicted beliefs as input, and shows them to the screen during training. This is totally optional, but very useful if you want to understand what is happening in your training run.
    % visualization function
    function viz(b_i)
        % here, b_i is a cell array of size nvals x nvars
        M = 5; % 5
        for n=1:M
            [ly lx lz] = size(ims_train{n});
            subplot(3,M,n    ); miximshow(reshape(b_i{n}',ly,lx,nvals),nvals);
            subplot(3,M,n+  M); imshow(ims_train{n})
            subplot(3,M,n+2*M); miximshow(reshape(labels_train{n},ly,lx),nvals);

        end
        xlabel('top: marginals  middle: input  bottom: labels')
        drawnow
    end
% Now, we choose what learning method to use. Here, we choose truncated fitting with the clique logistic loss. We use 5 iterations of TRW inference. Here, we use 'trwpll' to indicate to use the multithreaded TRW code. You will probably have to call 'compile_openmp' to make this work. Otherwise, you could just switch to 'trunc_cl_trw_5', which uses the non-parallel code.
loss_spec = 'trunc_cl_trwpll_5';
% Finally, we actually train the model. This takes about an hour and a half on an 8-core machine. You should have at least 4-8GB of memory.
p = gcp;
% delete(p);
% parpool local

fprintf('training the model (this is slow!)...\n')
crf_type  = 'linear_linear';
options.viz         = @viz;
options.print_times = 0; % since this is so slow, print stuff to screen
options.gradual     = 0.5; % use gradual fitting
options.maxiter     = 100;
options.rho         = rho;
options.reg         = 1e-4;
options.opt_display = 0;
options.maxtime     = 3600;

p = train_crf(feats_train,efeats_train,labels_train,models_train,loss_spec,crf_type,options);

% ans = 
% 
%     F: [8x100 double]
%     G: [64x22 double]

% The result is a structure array p. It contains two matrices. The first, F, determines the univariate potentials. Specifically, the vector of log-potentials for node i is given by multiplying F with the features for node i. Since there are 100 univariate features, this is a 8x100 matrix. Similarly, G determines the log-potentials for the edge interactions. Since there are 22 edge features and 64=8*8 pairwise values, this is a 64x22 matrix.

%SAVE THE MODEL!!!
save([folder_name, filesep, 'model.mat'], 'p');

%save params too!
save([folder_name, filesep, 'params.mat',], 'nvals', 'rho', 'rez', 'imsdir', 'labdir', 'feat_params', ...
    'edge_params', 'foot', 'samples', 'N', 'k', 'p', 'crf_type');


fprintf('get the marginals for test images...\n');
close all

E = ones(length(feats_test), 1);
E_rand = ones(length(feats_test), 1);
E_jaccard = ones(length(feats_test), 1);
E_pxe = ones(length(feats_test), 1);
E_impToe = ones(length(feats_test), 1);
E_impMFF = ones(length(feats_test), 1);
E_impLFF = ones(length(feats_test), 1);
E_impHeel = ones(length(feats_test), 1);
E_impOther = ones(length(feats_test), 1);

for n = 1:length(feats_test)
%     figure(n);
    [b_i, b_ij] = eval_crf(p,feats_test{n},efeats_test{n},models_test{n},loss_spec,crf_type,rho);
    
    
%     [ly, lx, lz] = size(labels_test{n});
%     [~,x_pred] = max(b_i,[],1);
%     x_pred = reshape(x_pred,ly,lx);
% 
%     [ly, lx, lz] = size(labels0_test{n});
%     x       = labels0_test{n};
%     % upsample predicted images to full resolution
%     x           = imresize(x,          size(tsd{n}{1}), 'nearest');
%     x_pred      = imresize(x_pred,     size(tsd{n}{1}), 'nearest');
%     ims_test{n} = imresize(ims_test{n},size(tsd{n}{1}));
% %     x_pred  = imresize(x_pred,size(x),'nearest');
%     E(n)   = sum(x_pred(x(:)>0)~=x(x(:)>0));
%     T(n)   = sum(x(:)>0);
%     
%     for tsdn = 1 : numel(tsd{n})
%         tsd{n}{tsdn} = imresize(tsd{n}{tsdn}, size(x),'nearest');
%     end

    

    [ly lx lz] = size(labels_test{n});
    [~,x_pred] = max(b_i,[],1);
    x_pred = reshape(x_pred,ly,lx);
    
    [ly lx lz] = size(labels0_test{n});
    x       = labels0_test{n};
    % upsample predicted images to full resolution
    x_pred  = imresize(x_pred,size(x),'nearest');
    E(n)   = sum(x_pred(x(:)>0)~=x(x(:)>0));
    T(n)   = sum(x(:)>0);
    
    
%     x_pred = x_pred(6:end-5, 6:end-5);
%     x = x(6:end-5, 6:end-5);
%     tsd_test{n} = tsd_test{n}(6:end-5, 6:end-5);

    
    [ly, lx, lz] = size(ims_test{n});
    subplot(2,3,1)
    miximshow(reshape(b_i',ly,lx,nvals),nvals);
    subplot(2,3,2)
    imshow(ims_test{n})
    subplot(2,3,3)
    miximshow(reshape(labels_test{n},ly,lx),nvals);

    [ly, lx, lz] = size(labels0_test{n});
    subplot(2,3,4)
    miximshow(reshape(x_pred,ly,lx),nvals);
    subplot(2,3,5)
    imshow(ims_test{n})
    subplot(2,3,6)
    miximshow(reshape(labels0_test{n},ly,lx),nvals);
    drawnow
    savefig([folder_name, filesep, 'classify_', int2str(n), '.fig']);
    
    [~, ImpMap] = CalculateImpulse(tsd_test{n});
    
    [E_jaccard(n), E_rand(n), E_pxe(n), E_impToe(n), E_impMFF(n), E_impLFF(n), E_impHeel(n), E_impOther(n)] =  Evaluate(x, x_pred, ImpMap);
%                             ^ This is pixel-wise error
end
fprintf('Total pixelwise error on test data: %f \n', sum(E)/sum(T))

fprintf('\nSimilarity (higher is better):\n');
fprintf('\t                                Min      | Max      | Mean     | Median   | Variance\n');
fprintf('\tJaccard Score                   %1.6f | %1.6f | %1.6f | %1.6f | %1.6f\n',  min(E_jaccard),  max(E_jaccard),  mean(E_jaccard),  median(E_jaccard),  var(E_jaccard));
fprintf('\tRand Index                      %1.6f | %1.6f | %1.6f | %1.6f | %1.6f\n',  min(E_rand),     max(E_rand),     mean(E_rand),     median(E_rand),     var(E_rand));
fprintf('\nPercentage Error (lower is better):\n');
fprintf('\t                                Min      | Max      | Mean     | Median   | Variance\n');
fprintf('\tPixelwise Error                 %1.6f | %1.6f | %1.6f | %1.6f | %1.6f\n', min(E_pxe),      max(E_pxe),      mean(E_pxe),      median(E_pxe),      var(E_pxe));
fprintf('\tImpulse Difference, Toe         %1.6f | %1.6f | %1.6f | %1.6f | %1.6f\n', min(E_impToe),   max(E_impToe),   mean(E_impToe),   median(E_impToe),   var(E_impToe));
fprintf('\tImpulse Difference, MFF         %1.6f | %1.6f | %1.6f | %1.6f | %1.6f\n', min(E_impMFF),   max(E_impMFF),   mean(E_impMFF),   median(E_impMFF),   var(E_impMFF));
fprintf('\tImpulse Difference, LFF         %1.6f | %1.6f | %1.6f | %1.6f | %1.6f\n', min(E_impLFF),   max(E_impLFF),   mean(E_impLFF),   median(E_impLFF),   var(E_impLFF));
fprintf('\tImpulse Difference, Heel        %1.6f | %1.6f | %1.6f | %1.6f | %1.6f\n', min(E_impHeel),  max(E_impHeel),  mean(E_impHeel),  median(E_impHeel),  var(E_impHeel));
fprintf('\tImpulse Difference, Unaccounted %1.6f | %1.6f | %1.6f | %1.6f | %1.6f\n', min(E_impOther), max(E_impOther), mean(E_impOther), median(E_impOther), var(E_impOther));

echo off;
diary off;
end

